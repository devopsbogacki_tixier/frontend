const express = require('express');
const axios = require('axios');
const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));

// Afficher la page d'accueil
app.get('/', (req, res) => {
    res.render('index');
});

// Gérer la soumission du formulaire de calcul
app.post('/calculate', async (req, res) => {
    const { operand1, operand2, operator } = req.body;

    try {
        const response = await axios.post('http://localhost:10003/api/calculate', null, {
            params: {
                operand1: parseInt(operand1),
                operand2: parseInt(operand2),
                operator
            }
        });

        res.render('index', { result: response.data.result });
    } catch (error) {
        res.render('index', { error: 'Une erreur s\'est produite lors du calcul.' });
    }
});

// Afficher les 5 dernières opérations
app.get('/history', async (req, res) => {
    try {
        const response = await axios.get('http://localhost:10003/api/last5');
        res.render('history', { operations: response.data });
    } catch (error) {
        res.render('history', { error: 'Une erreur s\'est produite lors de la récupération de l\'historique.' });
    }
});

app.listen(port, () => {
    console.log(`Application en cours d'exécution sur http://localhost:${port}`);
});
