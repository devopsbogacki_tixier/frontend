# Utiliser l'image officielle Node.js comme image de base
FROM node:14

# Créer un répertoire de travail
WORKDIR /app

# Copier package.json et package-lock.json
COPY package*.json ./

# Installer les dépendances de l'application
RUN npm install

# Copier le reste des fichiers de l'application
COPY . .

# Exposer le port sur lequel l'application s'exécute
EXPOSE 10003

# Démarrer l'application
CMD ["node", "app.js"]
